#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import requests
import csv
from queue import Queue
from threading import Thread
from bs4 import BeautifulSoup

SITE_URL = "https://www.similarweb.com"

# Чтобы не считали ботом
REQUST_HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36"
}

CHART_CLASS = ".trafficSourcesChart"
CHART_ITEM_CLASS = ".trafficSourcesChart-item"
CHART_ITEM_ATTRIBUTE = "data-key"
CHART_ITEM_VALUE_CLASS = ".trafficSourcesChart-value"

def write_csv(path, rows):
    with open(path, mode='w', encoding='utf-8') as w_file:
        file_writer = csv.writer(w_file, delimiter=",", lineterminator="\r")
        file_writer.writerows(rows)

def parse_domain(domain, rows):
    url = f"{SITE_URL}/website/{domain}/#overview"
    response = requests.get(url, headers=REQUST_HEADERS)

    if response.status_code != 200:
        print(f"{url} error {response.status_code}")
        return

    soup = BeautifulSoup(response.text, 'html.parser')
    chart = soup.select_one(CHART_CLASS)

    if chart is None:
        return

    items = chart.select(CHART_ITEM_CLASS)

    if len(rows) == 0:
        fieldnames = [name[CHART_ITEM_ATTRIBUTE] for name in items]
        fieldnames = ["Domain", *fieldnames]
        rows.append(fieldnames)

    data = [domain]
    for item in items:
        value = item.select_one(CHART_ITEM_VALUE_CLASS).text
        data.append(value)
    rows.append(data)

def worker(q, rows):
    while True:
        if q.empty():
            sys.exit()
        domain = q.get()
        parse_domain(domain, rows)
        q.task_done()

def main(domains):

    q = Queue()
    rows = []

    for domain in domains:
        q.put(domain)

    threads = []
    for x in range(0, min(3, len(domains))):
        thread = Thread(target=worker, args=(q, rows))
        threads.append(thread)
        thread.start()

    for t in threads:
        t.join()

    if len(rows):
        print(rows)
        write_csv('./data.csv', rows)
    else:
        sys.exit(1)


if __name__ == '__main__':
    args = sys.argv[1:]

    if len(args) < 1:
        print("example: main.py vk.com yandex.ru etc")

    main(args)
